package com.pryanik.procrastination.support;

public class Constants {
    private Constants() {
        throw new AssertionError();
    }

    static final String PREF_COINS = "pref_coins";

    static final String ADMOB_ID = "YOUR_ADMOB_ACCOUNT_ID_HERE";
    static final String AD_MOB_REWARDED_VIDEO_ID = "YOUR_ADMOB_ADD_ID";

    static final int REWARD_FOR_VIDEO = 20;
}