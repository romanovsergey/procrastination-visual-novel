package com.pryanik.procrastination.xmlParser;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Node {
    private final String id;
    private final String next;
    private final String pers;
    private final String background;
    private List<String> phrases;
    private List<Choice> choices;
    private List<Sprite> sprites;
    private boolean gone;
    private String condition;

    Node(String id, String next, String pers, String background, String condition) {
        this.id = id;
        this.next = next;
        this.pers = pers;
        this.background = background;
        this.phrases = new ArrayList<>(8);
        this.sprites = new ArrayList<>(4);
        this.gone = false;
        this.condition = condition;
    }

    public String getId() {
        return id;
    }

    public String getNext() {
        return next;
    }

    public String getPers() {
        return pers;
    }

    public String getBackground() {
        return background;
    }

    public List<String> getPhrases() {
        return phrases;
    }

    public List<Choice> getChoices() {
        return choices;
    }

    public List<Sprite> getSprites() {
        return sprites;
    }

    public boolean isGone() {
        return gone;
    }

    public void setGone() {
        this.gone = true;
    }

    public String getCondition() {
        return condition;
    }

    void addPhrase(@NotNull String phrase) {
        String[] bufPhrs = phrase.split(XMLParseDialogues.splitter);
        for (String item : bufPhrs)
            phrases.add(item.trim());
    }

    void addChoice(Choice ch) {
        if (choices == null)
            choices = new ArrayList<>(XMLParseDialogues.maxNumChoice);
        choices.add(ch);
    }

    void addSprite(Sprite sp) {
        sprites.add(sp);
    }

    @Override
    @Contract(value = "null -> false", pure = true)
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return Objects.equals(id, node.id) &&
                Objects.equals(next, node.next) &&
                Objects.equals(pers, node.pers) &&
                Objects.equals(background, node.background) &&
                Objects.equals(phrases, node.phrases) &&
                Objects.equals(choices, node.choices);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, next, pers, background, phrases, choices);
    }
}