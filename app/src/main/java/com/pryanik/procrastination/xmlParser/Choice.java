package com.pryanik.procrastination.xmlParser;

public class Choice {
    private final String next;
    private String phrase;
    private String condition;

    Choice(String next, String condition) {
        if (next == null)
            throw new IllegalArgumentException("В choice обязательный атрибут next!");
        this.next = next;
        this.phrase = null;
        this.condition = condition;
    }

    public String getPhrase() {
        return phrase;
    }

    public String getNext() {
        return next;
    }

    public String getCondition() {
        return condition;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }
}