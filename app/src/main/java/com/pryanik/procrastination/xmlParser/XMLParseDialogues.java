package com.pryanik.procrastination.xmlParser;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class XMLParseDialogues {
    static final int maxNumChoice = 4;
    static final String splitter = "\\\\";
    private static final Emotions defaultEmotion = Emotions.JOY;
    private final List<Node> nodes;
    private final String[] avlbImages;
    private final String[] avlbSprites;

    enum Emotions {
        JOY("1"), ANGER("2"), SADNESS("3"), FEAR("4"), SHAME("5");
        private String str;

        Emotions(String str) {
            this.str = str.toLowerCase();
        }

        @Contract(pure = true)
        public String getStr() {
            return str;
        }

        @Nullable
        public static Emotions getByStr(String emt) {
            if (emt == null)
                return defaultEmotion;
            emt = emt.toLowerCase();
            for (Emotions e : Emotions.values()) {
                if (emt.equals(e.str))
                    return e;
            }
            return null;
        }
    }

    public XMLParseDialogues(InputStream xml, String[] backgrounds, String[] sprites) {
        nodes = new ArrayList<>();
        avlbImages = backgrounds;
        Arrays.sort(avlbImages);
        avlbSprites = sprites;
        Arrays.sort(avlbSprites);
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            XMLHandler handler = new XMLHandler();
            parser.parse(xml, handler);
        } catch (ParserConfigurationException | SAXException e) {
            throw new IllegalArgumentException("Невозможно создать парсер! Ошибка:" + e.getLocalizedMessage());
        } catch (IOException e) {
            throw new IllegalArgumentException("Невозможно открыть xml-файл! Ошибка:" + e.getLocalizedMessage());
        }
    }

    public List<Node> getNodes() {
        return nodes;
    }

    private class XMLHandler extends DefaultHandler {
        StringBuilder phrase;
        Node curNode;
        Choice curChoice;
        Set<String> setId;
        boolean aboveIsNext;

        @Override
        public void startDocument() {
            phrase = new StringBuilder();
            setId = new HashSet<>();
            aboveIsNext = false;
        }

        @Override
        public void startElement(String uri, String localName, String qName, @NotNull Attributes attributes) {
            String cond = attributes.getValue("cond");

            // NODE
            if ("node".equals(qName)) {
                // Id
                String id = attributes.getValue("id");
                if (id != null) {
                    int preLen = setId.size();
                    setId.add(id);
                    if (preLen == setId.size())
                        throw new IllegalArgumentException("Невозможно создание блоков node с одинаковыми id=" + id);
                }
                // Next
                //TODO: опработать над ошибками
                else if (aboveIsNext)
                    throw new IllegalArgumentException("Недостижимый код!");
                String next = attributes.getValue("next");
                aboveIsNext = next != null;
                // Pers
                String pers = attributes.getValue("pers");
                // Back
                String back = attributes.getValue("rear");
                if (back != null && Arrays.binarySearch(avlbImages, back) < 0)       // Если картинки нет в массиве доступных
                    throw new IllegalArgumentException("Блок node с ID=" + id + " содержит некорректное значение атрибута rear!");
                // Создаём node и добавляем в список
                curNode = new Node(id, next, pers, back, cond);
                nodes.add(curNode);

                // CHOICE
            } else if ("choice".equals(qName)) {
                // Next
                String next = attributes.getValue("next");
                if (curNode.getChoices() == null)
                    curNode.addPhrase(normalPhrase());
                // Choice
                if (curNode.getChoices() != null && curNode.getChoices().size() == maxNumChoice)
                    throw new IllegalArgumentException("Невозможно добавить больше " + maxNumChoice + " выборов!");
                curChoice = new Choice(next, cond);
                curNode.addChoice(curChoice);

                // SPRITE
            } else if ("sprite".equals(qName)) {
                // Src
                String src = attributes.getValue("src");
                if (src == null)
                    throw new IllegalArgumentException("Атрибут src не может быть пустым!");
                // Feel
                Emotions feel = Emotions.getByStr(attributes.getValue("feel"));
                if (feel == null)
                    throw new IllegalArgumentException("Эмоции нет в списке доступных!");
                if (Arrays.binarySearch(avlbSprites, src + "_" + feel.getStr() + ".png") < 0)
                    throw new IllegalArgumentException("Несуществующий спрайт!");
                // Pos
                float pos = Float.parseFloat(attributes.getValue("pos"));
                Sprite sprite = new Sprite(src, feel, pos);
                curNode.addSprite(sprite);

            } else if (!"resources".equals(qName))      // Если тег не node, не choice, не sprite и не resources
                throw new IllegalArgumentException(qName + " - Некорректный тег");
            // Обнуляем строку
            phrase = new StringBuilder();
        }

        @Override
        public void endElement(String uri, String localName, String qName) {
            if ("node".equals(qName)) {
                if (curNode.getNext() == null && curNode.getCondition() != null)
                    throw new IllegalArgumentException("Блок node с ID=" + curNode.getId() + " содержит условие и не имеет атрибута next!");
                // Проверяем, чтоб хотя бы один выбор был без условия
                if (curNode.getChoices() != null) {
                    int i = 0;
                    for (Choice ch : curNode.getChoices()) {
                        if (ch.getCondition() != null)
                            i++;
                    }
                    if (i == curNode.getChoices().size())
                        throw new IllegalArgumentException("Блок node с ID=" + curNode.getId() + " содержит условия на всех выборах!");
                }
                if (curNode.getChoices() == null)
                    curNode.addPhrase(normalPhrase());
                curNode = null;
            } else if ("choice".equals(qName)) {
                curChoice.setPhrase(normalPhrase());
                curChoice = null;
            }
        }

        @Override
        public void characters(@NotNull char[] ch, int start, int length) {
            String addPhrase = new String(ch, start, length).trim();
            if (!addPhrase.equals(""))
                phrase.append(" ").append(addPhrase);
        }

        @NotNull
        private String normalPhrase() {
            if (phrase.length() == 0)    // Пробелов быть не может из за trim в characters()
                return "";
            return phrase.deleteCharAt(0).toString();
        }
    }
}