package com.pryanik.procrastination.xmlParser;

public class Sprite {
    private final String src;
    private final XMLParseDialogues.Emotions feel;
    private final float pos;

    Sprite(String src, XMLParseDialogues.Emotions feel, float pos) {
        this.src = src;
        this.feel = feel;
        this.pos = pos;
    }

    public String getSrc() {
        return src;
    }

    public XMLParseDialogues.Emotions getFeel() {
        return feel;
    }

    public float getPos() {
        return pos;
    }
}