package com.pryanik.procrastination;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

public class MenuActivity extends AppCompatActivity implements View.OnTouchListener {
    public static final String GAME_START = "gameStart";
    public static final int NEW_GAME = 1;
    public static final int CONTINUE_GAME = 2;
    public static SharedPreferences saves;
    static final String APP_SAVES = "saves";
    static final String APP_CUR_FRAME = "frame";
    public static final String APP_CUR_IMAGE = "image";
    public static final long TIME_ANIM = 100;
    ImageView btnContinue, btnNewGame, btnSettings, btnChapter, btnExit;
    public MediaPlayer mp;
    public SoundPool sp;
    private int soundClick;
    private long startTimeAnim;
    private long timeClick;
    private boolean flag;
    Handler h;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //hideNavigation();
        setContentView(R.layout.activity_menu);
        saves = getSharedPreferences(APP_SAVES, Context.MODE_PRIVATE);

        btnContinue = findViewById(R.id.btnContinue);
        btnContinue.setOnTouchListener(this);
        btnNewGame = findViewById(R.id.btnNewGame);
        btnNewGame.setOnTouchListener(this);
        btnSettings = findViewById(R.id.btnSettings);
        btnSettings.setOnTouchListener(this);
        btnChapter = findViewById(R.id.btnChapter);
        btnChapter.setOnTouchListener(this);
        btnExit = findViewById(R.id.btnExit);
        btnExit.setOnTouchListener(this);

        mp = MediaPlayer.create(this, R.raw.start_menu_music);
        AudioAttributes attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC).build();
        sp = new SoundPool.Builder().setAudioAttributes(attributes).build();
        soundClick = sp.load(this, R.raw.click_sound, 1);
        h = new Handler();
        flag = false;
    }

    public void onStartClick(@NotNull View view) {
        if (!flag)
            flag = true;
        else if (System.currentTimeMillis() - timeClick < 5000)
            return;
        timeClick = System.currentTimeMillis();
        soundClickPlay();
        int gameStart;
        Intent intent = new Intent(this, MainActivity.class);
        switch (view.getId()) {
            case R.id.btnNewGame:
                gameStart = NEW_GAME;
                break;
            case R.id.btnContinue:
                gameStart = CONTINUE_GAME;
                break;
            default:
                throw new IllegalArgumentException("Критическая ошибка!");
        }
        intent.putExtra(GAME_START, gameStart);
        startActivity(intent);
        mp.pause();
    }

    public void onSettingsClick(@NotNull View view) {
        soundClickPlay();
        /*Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);*/
    }

    public void onChapterClick(@NotNull View view) {
        soundClickPlay();
    }

    public void onExitClick(@NotNull View view) {
        soundClickPlay();
        mp.stop();
        System.exit(0);
    }

    public void hideNavigation() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideNavigation();
        if (saves.getInt(APP_CUR_FRAME, -1) == -1)
            btnContinue.setVisibility(View.GONE);
        else btnContinue.setVisibility(View.VISIBLE);
        mp.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mp.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mp.release();
        sp.release();
    }

    private void soundClickPlay() {
        sp.play(soundClick, 1, 1, 0, 0, 1);
    }

    @Override
    public boolean onTouch(View v, @NotNull MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startTimeAnim = System.currentTimeMillis();
                h.post(() -> v.animate().scaleX(0.9f).scaleY(0.9f).setDuration(TIME_ANIM).start());
                break;
            case MotionEvent.ACTION_UP:
                long curTimeAnim = System.currentTimeMillis() - startTimeAnim;
                curTimeAnim = TIME_ANIM - curTimeAnim;
                h.postDelayed(() -> v.animate().scaleX(1f).scaleY(1f).setDuration(TIME_ANIM).start(), curTimeAnim < 0 ? 0 : curTimeAnim);
                break;
        }
        return false;
    }
}